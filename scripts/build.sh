#!/bin/bash

cd src/

gox -output "../dist/dioderCLI_{{.OS}}_{{.Arch}}" -parallel=2 -verbose -os="${OPERATING_SYSTEM}" -ldflags="-s -w"

cd ..
