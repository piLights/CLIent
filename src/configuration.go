package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	homedir "github.com/mitchellh/go-homedir"
)

const cliConfigurationDirectory = ".piLights"

type Configuration struct {
	Address           string
	Debug             bool
	UpdateURL         string
	ConfigurationFile string
	Password          string
	IPv4Only          bool
	IPv6Only          bool
}

var CLIConfiguration Configuration

func (config *Configuration) WriteConfigurationToFile(fileName string) error {

	serializedConfiguration, error := json.Marshal(config)
	if error != nil {
		return error
	}

	error = ioutil.WriteFile(fileName, serializedConfiguration, os.ModePerm)

	return error
}

func NewConfiguration(fileName string, allowEmpty bool) error {
	if fileName == "" {
		if allowEmpty {
			CLIConfiguration = Configuration{}
			return nil
		}

		// Try to load the configuration from the home-directory of the user
		directoryPath, err := homedir.Dir()
		if err != nil {
			return err
		}

		fileName = directoryPath + "/" + cliConfigurationDirectory + "/config"

		// Check if the configurationFile exists
		_, err = os.Stat(fileName)
		if os.IsNotExist(err) {
			return err
		}
	}

	content, error := ioutil.ReadFile(fileName)
	if error != nil {
		return error
	}

	error = json.Unmarshal(content, &CLIConfiguration)

	return error
}
