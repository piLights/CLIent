package main

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/urfave/cli"
	"gitlab.com/piLights/proto"
)

var logCommand = cli.Command{
	Name:        "log",
	Usage:       "log",
	Description: "Retrieve the log from the server",
	Action:      retrieveLog,
}

func retrieveLog(ctx *cli.Context) error {
	// @ToDo: Build the logRequest dynamically
	logRequest := &LighterGRPC.LogRequest{
		LogLevel: 1,
		Amount:   100,
		Password: "",
	}

	stream, error := lighterClient.LoadServerLog(context.Background(), logRequest)
	if error != nil {
		return error
	}

	for {
		feature, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			return error
		}
		fmt.Println(time.Unix(0, feature.Time), feature.Message)
	}

	return nil
}
