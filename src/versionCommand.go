package main

import (
	"context"
	"fmt"

	"github.com/urfave/cli"
	"gitlab.com/piLights/proto"
)

var versionCommand = cli.Command{
	Name:        "version",
	Usage:       "version",
	Description: "Print the version of backend & CLI and exit",
	Action:      printVersion,
}

func printVersion(ctx *cli.Context) error {
	request := &LighterGRPC.Request{}

	backendVersion, error := lighterClient.Version(context.Background(), request)
	if error != nil {
		return error
	}

	fmt.Println("Backend-Version: ", backendVersion.VersionCode)
	fmt.Println("CLI-Version: ", version)

	return nil
}
