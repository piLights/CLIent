package main

import (
	"context"
	"fmt"

	"github.com/urfave/cli"
	"gitlab.com/piLights/proto"
)

var configurationCommand = cli.Command{
	Name:        "configuration",
	Usage:       "configuration",
	Description: "Retrieve the configuration from the server",
	Action:      loadConfiguration,
}

func loadConfiguration(ctx *cli.Context) error {
	request := &LighterGRPC.Request{}

	config, error := lighterClient.LoadServerConfiguration(context.Background(), request)
	if error != nil {
		return error
	}

	fmt.Println(config)

	return nil
}
