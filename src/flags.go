package main

import "github.com/urfave/cli"

var applicationFlags = []cli.Flag{
	cli.StringFlag{
		Name:  "writeConfiguration",
		Usage: "Write the current configuration to the given file",
	},
	cli.StringFlag{
		Name:        "configurationFile, c",
		Usage:       "Path to the configuration-file",
		Destination: &CLIConfiguration.ConfigurationFile,
	},
	cli.BoolFlag{
		Name:  "update, u",
		Usage: "Fetch the newest version",
	},
	cli.BoolFlag{
		Name:        "debug, d",
		Usage:       "Turn on the debug-mode",
		Destination: &CLIConfiguration.Debug,
	},
	cli.StringFlag{
		Name:        "password, P",
		Usage:       "The password to protect the endpoint",
		Destination: &CLIConfiguration.Password,
	},
	cli.StringFlag{
		Name:        "updateURL",
		Usage:       "Fetch the update from the given URL",
		Value:       UPDATEURL,
		Destination: &CLIConfiguration.UpdateURL,
	},
	cli.StringFlag{
		Name:        "address, a",
		Usage:       "The address of the piLights backend",
		Destination: &CLIConfiguration.Address,
	},
}
