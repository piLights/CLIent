package main

import (
	"context"
	"errors"
	"strconv"

	"github.com/urfave/cli"
	"gitlab.com/piLights/proto"
)

var colorCommand = cli.Command{
	Name:        "color",
	Usage:       "color [RED [GREEN [BLUE OPACITY]]]]",
	Description: "Control the color",
	Action:      setColor,
}

func setColor(ctx *cli.Context) error {
	argumentCount := ctx.NArg()

	colorMessage := &LighterGRPC.ColorMessage{}

	var number int

	switch argumentCount {
	case 4:
		number, _ = strconv.Atoi(ctx.Args().Get(3))
		colorMessage.Opacity = int32(number)
		fallthrough
	case 3:
		number, _ = strconv.Atoi(ctx.Args().Get(2))
		colorMessage.B = int32(number)
		fallthrough
	case 2:
		number, _ = strconv.Atoi(ctx.Args().Get(1))
		colorMessage.G = int32(number)
		fallthrough
	case 1:
		number, _ = strconv.Atoi(ctx.Args().Get(0))
		colorMessage.R = int32(number)
	}

	confirmation, error := lighterClient.SetColor(context.Background(), colorMessage)
	if error != nil {
		return error
	}

	if !confirmation.Success {
		return errors.New("unkown Error")
	}

	return nil
}
