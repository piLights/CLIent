package main

import "github.com/urfave/cli"

var setupCommand = cli.Command{
	Name:        "setup",
	Usage:       "setup",
	Description: "Setup the CLI",
	Action:      setupCLI,
}

func setupCLI(ctx *cli.Context) error {
	// @ToDO: Setup the address
	/*
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Address of the backend: ")
		address, _ := reader.ReadString('\n')
		fmt.Printf("Backend should be reachable at: %s", address)
	*/

	return nil
}
