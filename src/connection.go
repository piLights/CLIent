package main

import (
	"gitlab.com/piLights/proto"
	"google.golang.org/grpc"
)

var (
	serverConnection *grpc.ClientConn
	lighterClient    LighterGRPC.LighterClient
)

func connect() (LighterGRPC.LighterClient, error) {
	var error error
	serverConnection, error = grpc.Dial(CLIConfiguration.Address, grpc.WithInsecure())
	if error != nil {
		return nil, error
	}

	lighterClient = LighterGRPC.NewLighterClient(serverConnection)

	return lighterClient, nil
}
