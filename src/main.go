package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
)

const version = "debugVersion"

func main() {
	piLightsCLI := cli.NewApp()
	piLightsCLI.Name = "piLights CLI"
	piLightsCLI.Author = "Jannick Fahlbusch <git@jf-projects.de>"
	piLightsCLI.Version = version
	piLightsCLI.Commands = commands

	piLightsCLI.Flags = applicationFlags

	// Load the configuration
	err := NewConfiguration("", false)
	if err != nil {
		log.Fatal(err)
	}

	_, err = connect()
	if err != nil {
		log.Fatal(err)
	}
	defer serverConnection.Close()

	piLightsCLI.Action = func(c *cli.Context) error {
		if c.Bool("update") {
			startUpdate()
			return nil
		}

		return nil
	}

	piLightsCLI.Run(os.Args)
}
