package main

import "github.com/urfave/cli"

var commands = []cli.Command{
	//setupCommand,
	logCommand,
	colorCommand,
	configurationCommand,
	versionCommand,
}
